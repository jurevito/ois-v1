import java.util.Scanner;

public class Gravitacija {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("OIS je zakon!");
        double nadVisina=sc.nextDouble();
        double pospesek = bIzracun(nadVisina);
        
        System.out.println("Nadmorska visina je: " +nadVisina+" m");
		System.out.println("Gravitacijski pospesek je: " +pospesek+" m/s^2");
    }
    
    public static double bIzracun(double nadVisina){
        double pospesek=(6.674*5.972*Math.pow(10,13))/((6.371*Math.pow(10,6)+nadVisina)*(6.371*Math.pow(10,6)+nadVisina));
        return pospesek;
    }
}